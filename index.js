var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const axios = require('axios');
const path = require('path');

const TIMEOUT = 5 * 1000

app.use(express.static('static'));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));

app.get('/', function(req, res){
    res.render('index');
});

app.get('/dashboard', function(req, res){
    res.render('dashboard');
});

app.get('/logs', function(req, res){
    res.render('logs');
});

app.get('/stats', function(req, res){
    res.render('stats');
});

app.get('/settings', function(req, res){
    res.render('settings');
});

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
    socket.on('newLog', function(msg){
        socket.broadcast.emit('newLog', msg);
    })
    socket.on('jphOk', function(){
        socket.broadcast.emit('jphOk')
    })
    socket.on('jphKo', function(){
        socket.broadcast.emit('jphKo')
    })
    socket.on('jphError', function(){
        socket.broadcast.emit('jphError')
    })
});

function testJph() {
    axios.get('https://jsonplaceholder.typicode.com/albums')
    .then(response => {
        if(response.status === 200){
            io.emit('jphOk');
            io.emit('newLog', 'Jsonplaceholder API is up and running !');
        }else{
            io.emit('jphKo');
            io.emit('newLog', 'Jsonplaceholder responds without code 200 !');
        }
    })
    .catch(error => {
        io.emit('jphError', error);
        io.emit('newLog', 'Jsonplaceholder responds with an error !');
    });
    setTimeout(testJph, TIMEOUT);
}

function testGoogle() {
    axios.get('https://www.google.com?q=monitoring')
    .then(response => {
        if(response.status === 200){
            io.emit('googleOk');
            io.emit('newLog', 'Google is up and running !');
        }else{
            io.emit('googleKo');
            io.emit('newLog', 'Google is up and running !');
        }
    })
    .catch(error => {
        io.emit('googleError', error);
        io.emit('newLog', 'Google is up and running !');
    });
    setTimeout(testGoogle, TIMEOUT);
}

function testAmazon() {
    axios.get('https://www.amazon.com/')
    .then(response => {
        if(response.status === 200){
            io.emit('amazonOk');
            io.emit('newLog', 'Amazon is up and running !');
        }else{
            io.emit('amazonKo');
            io.emit('newLog', 'Amazon is up and running !');
        }
    })
    .catch(error => {
        io.emit('amazonError', error);
        io.emit('newLog', 'Amazon is up and running !');
    });
    setTimeout(testAmazon, TIMEOUT);
}

http.listen(3000, function(){
    console.log('Server is listening on port : 3000 !');
    setTimeout(testJph, TIMEOUT);
    setTimeout(testGoogle, TIMEOUT);
    setTimeout(testAmazon, TIMEOUT);
});